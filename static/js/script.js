
$('.historia-content').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  infinite: false,
  asNavFor: '.historia-nav'
});

$('.historia-nav').slick({
  slidesToShow: 10,
  slidesToScroll: 10,
  asNavFor: '.historia-content',
  adaptiveHeight: true,
  dots: false,  
  centerMode: false,
  infinite: false,
  focusOnSelect: true,
  slide: 'div',
  responsive: [
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 3,
        centerMode: true,
        focusOnSelect: false
      }
    }
  ]
});


$('#derecha').click(function () { 
  $('.historia-content').slick("slickNext");
});

$('#izquierda').click(function () { 
  $('.historia-content').slick("slickPrev");
});


$('.historia-nav__item').click(function(){
   $('.historia-nav__item').removeClass('slick-current')
   $(this).addClass('slick-current');
});





//$('#izquierda').slick("Next");

//$('#derecha').slick("Prev");
